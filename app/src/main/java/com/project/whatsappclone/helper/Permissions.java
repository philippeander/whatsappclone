package com.project.whatsappclone.helper;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class Permissions {

    private static final int MIN_SDK_VERSION = 23;

    public static boolean ValidatePermissions(Activity activity, String[] permissions, int requestCode){
        if(Build.VERSION.SDK_INT >= MIN_SDK_VERSION){
            List<String> whiteList = new ArrayList<String>();

            for(String permission : permissions){
                Boolean IsPermissionValid = ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;
                if(!IsPermissionValid){
                    whiteList.add(permission);
                }
            }

            //if whiteList is empty
            if(whiteList.isEmpty()) return true;

            //convert whiteList to Array
            String[] missingPermissions = new String[whiteList.size()];
            whiteList.toArray(missingPermissions);

            //Solicita permissão
            ActivityCompat.requestPermissions(activity, missingPermissions, requestCode);

        }
        return true;
    }

}
