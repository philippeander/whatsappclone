package com.project.whatsappclone.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.project.whatsappclone.R;

import java.util.HashMap;

public class Preferences {

    private Context context;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor_preferences;

    private static final String FILE_NAME = "whatsapp.preferences";
    private static final String ID_KEY = "userLogedId";
    private static final int FILE_MODE = 0;

    public static final String KEY_NOME = "name";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_TOKEN = "token";

    public Preferences(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(FILE_NAME, FILE_MODE);
        editor_preferences = sharedPreferences.edit();
    }

    public void SaveUserPreferences(String name, String phone, String token){
        editor_preferences.putString(KEY_NOME, name);
        editor_preferences.putString(KEY_PHONE, phone);
        editor_preferences.putString(KEY_TOKEN, token);
        editor_preferences.commit();
    }

    public void DataSave(String name){
        editor_preferences.putString(KEY_NOME, name);
        editor_preferences.commit();
    }

    public String GetIdKey(){
        return sharedPreferences.getString(ID_KEY, null);
    }

    public HashMap<String, String> getUserData(){
        HashMap<String, String> userData = new HashMap<>();

        userData.put(KEY_NOME, sharedPreferences.getString(KEY_NOME, null));
        userData.put(KEY_PHONE, sharedPreferences.getString(KEY_PHONE, null));
        userData.put(KEY_TOKEN, sharedPreferences.getString(KEY_TOKEN, null));

        return userData;
    }
}
