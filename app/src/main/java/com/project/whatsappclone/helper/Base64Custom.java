package com.project.whatsappclone.helper;

import android.util.Base64;

public class Base64Custom {

    public static String Base64_Encode(String text){
        return Base64.encodeToString(text.getBytes(), Base64.DEFAULT).replaceAll("(\\n|\\r)", "");
    }

    public static String Base64_Decode(String text){
        return new String(Base64.decode(text, Base64.DEFAULT));
    }

}
