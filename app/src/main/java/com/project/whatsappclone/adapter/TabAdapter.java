package com.project.whatsappclone.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.project.whatsappclone.fragment.ChatFragment;
import com.project.whatsappclone.fragment.ContactsFragment;

public class TabAdapter extends FragmentStatePagerAdapter{

    private static final String TAB_CHAT = "Chat";
    private static final String TAB_CONTACTS = "Contacts";

    private String[] TabsTitle = {TAB_CHAT, TAB_CONTACTS};


    public TabAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        switch (position){
            case 0:
                fragment = new ChatFragment();
                break;
            case 1:
                fragment = new ContactsFragment();
                break;

        }

        return fragment;
    }

    @Override
    public int getCount() {
        return TabsTitle.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return TabsTitle[position];
    }
}
