/*
    Revolutions will only last, if they are based on education.
 */
package com.project.whatsappclone.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;
import com.project.whatsappclone.Manifest;
import com.project.whatsappclone.R;
import com.project.whatsappclone.helper.Permissions;
import com.project.whatsappclone.helper.Preferences;

import java.util.HashMap;
import java.util.Random;

public class LoginActivity extends AppCompatActivity {

    private EditText edtTxt_UserName;
    private EditText edtTxt_ddiCode;
    private EditText edtTxt_dddCode;
    private EditText edtTxt_phoneNumber;
    private Button btn_registerUser;

    private Random rdm;

    Preferences preferences;

    //LIST ALL DANGEROUS PERMISSIONS
    private String[] requiredPermissions = new String[]{
            android.Manifest.permission.SEND_SMS
    };

    private static final String DDI_MASK = "+NN";
    private static final String DDD_MASK = "NN";
    private static final String PHONE_MASK = "N NNNN NNNN";

    private static final int TOKEN_MAX_VALUE = 9999;
    private static final int TOKEN_MIN_VALUE = 1000;
    private static final int REQUEST_CODE_PERMISSIONS = 1;

    private static final String MENSAGE_SEND_TOKEN = "WhatsApp verification code: ";
    private static final String SPACE = " ";
    private static final String NON_SPACE = "";
    private static final String PLUS_DDI = "+";

    private static final String ALERT_PERMISSION_TITLE = "permissions denied";
    private static final String ALERT_PERMISSION_MENSAGE = "To use this app, you must accept the permissions!";
    private static final String ALERT_PERMISSION_ERROR = "There was a problem sending the SMS, please try again!";
    private static final String ALERT_PERMISSION_CONFIRM = "OK";
    private static final String ALERT_PERMISSION_CANCEL = "NO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Permissions.ValidatePermissions(this, requiredPermissions, REQUEST_CODE_PERMISSIONS);
        rdm = new Random();
        preferences = new Preferences(LoginActivity.this);

        edtTxt_UserName = findViewById(R.id.edtText_enterName_id);
        edtTxt_ddiCode = findViewById(R.id.edtTxt_countryCode_id);
        edtTxt_dddCode = findViewById(R.id.edtTxt_stateCode_id);
        edtTxt_phoneNumber  = findViewById(R.id.edtTxt_PhoneNumber_id);
        btn_registerUser    = findViewById(R.id.btn_registerUser_id);

        CriateMasks();

        btn_registerUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnRegisterClicked();
            }
        });

    }

    private void CriateMasks(){
        SimpleMaskFormatter simpleMask_DDI_Code = new SimpleMaskFormatter(DDI_MASK);
        SimpleMaskFormatter simpleMask_DDD_Code = new SimpleMaskFormatter(DDD_MASK);
        SimpleMaskFormatter simpleMask_phoneNumber = new SimpleMaskFormatter(PHONE_MASK);

        MaskTextWatcher mask_countryCode = new MaskTextWatcher(edtTxt_ddiCode, simpleMask_DDI_Code);
        MaskTextWatcher mask_stateCode = new MaskTextWatcher(edtTxt_dddCode, simpleMask_DDD_Code);
        MaskTextWatcher mask_phoneNumber = new MaskTextWatcher(edtTxt_phoneNumber, simpleMask_phoneNumber);

        edtTxt_ddiCode.addTextChangedListener(mask_countryCode);
        edtTxt_dddCode.addTextChangedListener(mask_stateCode);
        edtTxt_phoneNumber.addTextChangedListener(mask_phoneNumber);

    }

    private void OnRegisterClicked(){

        String UserName     = edtTxt_UserName.getText().toString();
        String UserPhone    =
                        edtTxt_ddiCode.getText().toString() +
                        edtTxt_dddCode.getText().toString() +
                        edtTxt_phoneNumber.getText().toString();
        String UserPhoneReal = UserPhone.replace(PLUS_DDI, NON_SPACE);
        UserPhoneReal = UserPhoneReal.replace(SPACE, NON_SPACE);

        SaveDataValitation(UserName, UserPhoneReal, GenerateToken());
    }

    private void SaveDataValitation(String userName, String userPhone, String token){
        //Preferences preferences = new Preferences(LoginActivity.this);
        preferences.SaveUserPreferences(userName, userPhone, token);

        //PrintUserData();
        Boolean isSendSMS = SendSMS(PLUS_DDI + userPhone, MENSAGE_SEND_TOKEN + token);

        if(isSendSMS){
            Intent intent = new Intent(LoginActivity.this, ValidatorActivity.class);
            startActivity(intent);
            finish();
        }else{
            Toast.makeText(this, ALERT_PERMISSION_ERROR, Toast.LENGTH_LONG).show();
        }
    }

    private String GenerateToken(){
        int randomNumber = rdm.nextInt(TOKEN_MAX_VALUE - TOKEN_MIN_VALUE) + TOKEN_MIN_VALUE;
        String token = String.valueOf(randomNumber);
        return token;
    }

    private boolean SendSMS(String phoneNumber, String mensage){
        //Is necessary a manifest permition to send SMS like Internet permition
        try{
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNumber,null, mensage, null, null);

            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for(int result : grantResults){
            if(result == PackageManager.PERMISSION_DENIED){
                alert_validationPermission();
            }
        }
    }
    private void alert_validationPermission(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(ALERT_PERMISSION_TITLE.toUpperCase());
        builder.setMessage(ALERT_PERMISSION_MENSAGE);

        builder.setPositiveButton(ALERT_PERMISSION_CONFIRM, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    // EXTRA CODE HELPER ----------------------- EXTRA CODE HELPER

    private void PrintUserData(){
        HashMap<String, String> userData = preferences.getUserData();

        Log.i("userData",">>>> >>>>" +
                "\n USER NOME: " + userData.get(preferences.KEY_NOME) +
                "\n USER PHONE: " + userData.get(preferences.KEY_PHONE) +
                "\n USER TOKEN: " + userData.get(preferences.KEY_TOKEN));
    }
}
