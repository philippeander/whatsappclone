package com.project.whatsappclone.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;
import com.project.whatsappclone.R;
import com.project.whatsappclone.helper.Preferences;

import java.util.HashMap;

public class ValidatorActivity extends AppCompatActivity {

    private EditText edtTxt_codeValidation;
    private Button btn_Validate;

    private static final String TOKEN_MASK  = "NNNN";
    private static final String ALERT_VALID_TOKEN = "Welcome, Valid Token";
    private static final String ALERT_INVALID_TOKEN = "Error, Invalid Token";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validator);

        edtTxt_codeValidation = findViewById(R.id.edtTxt_codeValidation);
        btn_Validate = findViewById(R.id.btn_validate);

        CreateMasks();

        btn_Validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ValidateToken();
            }
        });

    }

    private void CreateMasks(){
        SimpleMaskFormatter simpleMask_token = new SimpleMaskFormatter(TOKEN_MASK);

        MaskTextWatcher mask_token = new MaskTextWatcher(edtTxt_codeValidation, simpleMask_token);

        edtTxt_codeValidation.addTextChangedListener(mask_token);
    }

    private void ValidateToken(){
        //recover user data
        Preferences preferences = new Preferences(ValidatorActivity.this);
        HashMap<String, String> userData = preferences.getUserData();

        String generatedToken = userData.get(preferences.KEY_TOKEN);
        String typedToken = edtTxt_codeValidation.getText().toString();

        if(typedToken.equals(generatedToken)){
            Toast.makeText(this, ALERT_VALID_TOKEN, Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this, ALERT_INVALID_TOKEN, Toast.LENGTH_SHORT).show();
        }
    }
}
