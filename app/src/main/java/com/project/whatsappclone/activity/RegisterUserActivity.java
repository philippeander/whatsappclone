package com.project.whatsappclone.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.project.whatsappclone.*;
import com.project.whatsappclone.config.FirebaseConfiguration;
import com.project.whatsappclone.helper.Base64Custom;
import com.project.whatsappclone.helper.Preferences;
import com.project.whatsappclone.model.User;

public class RegisterUserActivity extends AppCompatActivity {

    private EditText edtTxt_name;
    private EditText edtTxt_email;
    private EditText edtTxt_password;
    private Button btn_register;

    private User user;
    private FirebaseAuth authentication;

    private static final String MESSAGE_USER_REGISTERED = "User registered successfully!!";
    private static final String MESSAGE_ENTER_EXCEPTION = "Error: ";
    private static final String MESSAGE_ERROR_USER_REGISTERED = "The user could not be registered.";
    private static final String MESSAGE_PASSWORD_EXCEPTION = "Enter a stronger password with at least 6 characters containing letters and numbers.";
    private static final String MESSAGE_EMAIL_EXCEPTION = "The email you entered is invalid, please enter a new email.";
    private static final String MESSAGE_COLLISION_EXCEPTION = "E-mail already registered.";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        edtTxt_name = findViewById(R.id.edtTxt_name_id);
        edtTxt_email = findViewById(R.id.edtTxt_email_id);
        edtTxt_password = findViewById(R.id.edtTxt_password_id);
        btn_register = findViewById(R.id.btn_register_id);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateNewUser();
            }
        });
    }

    private void CreateNewUser(){
        user = new User();
        user.setName(edtTxt_name.getText().toString());
        user.setEmail(edtTxt_email.getText().toString());
        user.setPassword(edtTxt_password.getText().toString());
        RegisterUser();

    }

    private void RegisterUser(){
        authentication = FirebaseConfiguration.getFirebaseAuthentication();
        authentication.createUserWithEmailAndPassword(user.getEmail(), user.getPassword()).
                addOnCompleteListener(RegisterUserActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(RegisterUserActivity.this, MESSAGE_USER_REGISTERED, Toast.LENGTH_LONG).show();

                            //FirebaseUser firebaseUser = task.getResult().getUser();
                            //user.setId(firebaseUser.getUid());
                            String userId_encrypted = Base64Custom.Base64_Encode(user.getEmail());
                            //Log.i("USER", userId_encrypted);
                            user.setId(userId_encrypted);
                            user.Save();

                            Preferences preferences = new Preferences(RegisterUserActivity.this);
                            preferences.DataSave(userId_encrypted);

                            //authentication.signOut();
                            //finish();
                            OpenUserLogin();

                        }else {

                            String exceptionError = "";

                            try{
                                //throw => Lançar (Lançar exceção)
                                throw task.getException();
                            } catch (FirebaseAuthWeakPasswordException e){
                                exceptionError = MESSAGE_PASSWORD_EXCEPTION;
                            } catch (FirebaseAuthInvalidCredentialsException e) {
                                exceptionError = MESSAGE_EMAIL_EXCEPTION;
                            } catch (FirebaseAuthUserCollisionException e) {
                                exceptionError = MESSAGE_COLLISION_EXCEPTION;
                            } catch (Exception e) {
                                exceptionError = MESSAGE_ERROR_USER_REGISTERED;
                                e.printStackTrace();
                            }

                            Toast.makeText(RegisterUserActivity.this, MESSAGE_ENTER_EXCEPTION + exceptionError, Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void OpenUserLogin(){
        Intent intent = new Intent(RegisterUserActivity.this, LoginActivity_Email.class);
        startActivity(intent);
        finish();
    }
}
