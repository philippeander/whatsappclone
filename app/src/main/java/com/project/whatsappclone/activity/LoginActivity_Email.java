package com.project.whatsappclone.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.project.whatsappclone.*;
import com.project.whatsappclone.config.FirebaseConfiguration;
import com.project.whatsappclone.helper.Base64Custom;
import com.project.whatsappclone.helper.Permissions;
import com.project.whatsappclone.helper.Preferences;
import com.project.whatsappclone.model.User;

public class LoginActivity_Email extends AppCompatActivity {

    private EditText edtTxt_email;
    private EditText edtTxt_password;
    private Button btn_login;
    private TextView txt_register;

    private User user;
    private FirebaseAuth authentication;
    private DatabaseReference firebaseReference;

    //LIST ALL DANGEROUS PERMISSIONS
    private String[] requiredPermissions = new String[]{
            android.Manifest.permission.INTERNET
    };

    private static final int REQUEST_CODE_PERMISSIONS = 1;
    private static final String MESSAGE_LOGIN_SUCCESSFULLY = "Hello, login successfully!";
    private static final String MESSAGE_LOGIN_FAILED= "Erro, login is failed!";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_email);

        Permissions.ValidatePermissions(this, requiredPermissions, REQUEST_CODE_PERMISSIONS);
        Check_IsUserLogged();

        edtTxt_email = findViewById(R.id.edtTxt_email_id);
        edtTxt_password = findViewById(R.id.edtTxt_password_id);
        btn_login = findViewById(R.id.btn_login_id);
        txt_register = findViewById(R.id.txt_register_id);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                user = new User();
                user.setEmail(edtTxt_email.getText().toString());
                user.setPassword(edtTxt_password.getText().toString());
                LoginValidation();

            }
        });


    }

    public void OpenUserRegistration(View view){
        Intent intent = new Intent(LoginActivity_Email.this, RegisterUserActivity.class);
        startActivity(intent);
    }

    private void LoginValidation(){
        authentication = FirebaseConfiguration.getFirebaseAuthentication();
        authentication.signInWithEmailAndPassword(
                user.getEmail(),
                user.getPassword()
        ).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){

                    Preferences preferences = new Preferences(LoginActivity_Email.this);
                    String IdLogedUser = Base64Custom.Base64_Encode(user.getEmail());
                    preferences.DataSave(IdLogedUser);

                    OpenMainScreen();
                    Toast.makeText(LoginActivity_Email.this, MESSAGE_LOGIN_SUCCESSFULLY, Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(LoginActivity_Email.this, MESSAGE_LOGIN_FAILED, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void OpenMainScreen(){
        Intent intent = new Intent(LoginActivity_Email.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void Check_IsUserLogged(){
        authentication = FirebaseConfiguration.getFirebaseAuthentication();
        if(authentication.getCurrentUser() != null){
            OpenMainScreen();
        }
    }


}
