package com.project.whatsappclone.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.project.whatsappclone.R;
import com.project.whatsappclone.adapter.TabAdapter;
import com.project.whatsappclone.config.FirebaseConfiguration;
import com.project.whatsappclone.helper.Base64Custom;
import com.project.whatsappclone.helper.Preferences;
import com.project.whatsappclone.helper.SlidingTabLayout;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private FirebaseAuth authentication;

    private SlidingTabLayout slidingTabLayout;
    private ViewPager viewPager;

    private String contact_id;
    private DatabaseReference firebaseReference;

    private static final String NAME_USERS_TABLE = "users";
    private static final String TABLE_CONTACT = "contacts";
    private static final String ALERT_DIALOG_ADD_CONTACT_TITLE = "New contact";
    private static final String ALERT_DIALOG_ADD_CONTACT_MESSAGE = "User e-mail";
    private static final String ALERT_DIALOG_ADD_CONTACT_POSITIVE_BUTTON = "Add";
    private static final String ALERT_DIALOG_ADD_CONTACT_NEGATIVE_BUTTON = "Cancel";
    private static final String ALERT_DIALOG_ADD_CONTACT_NOT_FOUND = "insert a contact email";
    private static final String ALERT_DIALOG_ADD_CONTACT_NOT_REGISTRADED = "user not registered";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        authentication = FirebaseConfiguration.getFirebaseAuthentication();

        InitToolbar();
        InitTabs();

    }

    private void InitToolbar(){
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("WhatsApp");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //return super.onOptionsItemSelected(item);

        switch (item.getItemId()){
            case R.id.action_exit:
                SignOutApplication();
                return true;
            case R.id.action_settings:
                // Configs.
                return true;
            case R.id.parson_add:
                OpenRegistrationContact();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void OpenRegistrationContact(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

        //Dialog Config
        alertDialog.setTitle(ALERT_DIALOG_ADD_CONTACT_TITLE);
        alertDialog.setMessage(ALERT_DIALOG_ADD_CONTACT_MESSAGE);
        alertDialog.setCancelable(false);

        final EditText editText = new EditText(MainActivity.this);
        alertDialog.setView(editText);

        //Buttons Config
        alertDialog.setPositiveButton(ALERT_DIALOG_ADD_CONTACT_POSITIVE_BUTTON, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String emailcontact = editText.getText().toString();

                if(emailcontact.isEmpty()){
                    Toast.makeText(MainActivity.this, ALERT_DIALOG_ADD_CONTACT_NOT_FOUND, Toast.LENGTH_SHORT).show();
                }else{
                    //verify that the user is already registered in the app
                    contact_id = Base64Custom.Base64_Encode(emailcontact);

                    //recover firebase instance
                    firebaseReference = FirebaseConfiguration.getFirebase().child(NAME_USERS_TABLE).child(contact_id);

                    firebaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.getValue() != null){

                                //retrieve contact data to add


                                //retrieve user identifier logged in
                                //authentication.getCurrentUser().getEmail();
                                Preferences preferences = new Preferences(MainActivity.this);
                                String idUserLoged = preferences.GetIdKey();

                                firebaseReference = FirebaseConfiguration.getFirebase();
                                firebaseReference = firebaseReference.child(TABLE_CONTACT).
                                                                      child(idUserLoged).
                                                                      child(contact_id);
                                //firebaseReference.setValue();
                            }else{
                                Toast.makeText(MainActivity.this, ALERT_DIALOG_ADD_CONTACT_NOT_REGISTRADED, Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

            }
        });

        alertDialog.setNegativeButton(ALERT_DIALOG_ADD_CONTACT_NEGATIVE_BUTTON, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        alertDialog.create();
        alertDialog.show();
    }

    private void SignOutApplication(){
        authentication.signOut();
        Intent intent = new Intent(MainActivity.this, LoginActivity_Email.class);
        startActivity(intent);
        finish();
    }

    // -------------- TAB MANAGER ---------------------

    private void InitTabs(){
        slidingTabLayout = findViewById(R.id.stl_tabs);
        viewPager = findViewById(R.id.vp_page);

        //Sliding tab config
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setSelectedIndicatorColors(ContextCompat.getColor(this, R.color.colorAccent));

        //Adapter config
        TabAdapter tabAdapter = new TabAdapter(getSupportFragmentManager());
        viewPager.setAdapter(tabAdapter);

        slidingTabLayout.setViewPager(viewPager);

    }

}
